examples/udp-echo-server
================

This application is a simple UDP echo server, running on port 7.

The code was kept deliberately simple and everything runs in `main()`. For
a more complete example see `udp-echo-server-delay`.

Usage
=====

Use `shell` to build the app for the native platform and launch it.

On startup, the output of the ifconfig utility will be printed to the console.

By default the ECHO server logs lots of data to the console. Change the macro
`ENABLE_DEBUG` in `main.c` to modify this behavior.

Communicating
=============

The application does not set a proper ipv6 address. Use the link-local address
to communicate. Note that you must specify the interface when using link-local
addresses.

With netcat:
```
netcat -6u 'fe80::74b3:baff:fe20:ec27%tapbr0' 7
```

Of course, replace the address and tapbr0 for your actual address and interface.

