/**
 * @file
 * @brief       Simple UDP echo server
 *
 * @author      Juan I. Carrano <juan@carrano.com.ar>
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "net/sock/udp.h"
#include "net/ipv6/addr.h"

#define ENABLE_DEBUG (1)
#include "debug.h"

/* Some hacky debug macros */
#define STRING(s) #s
#define LOG(...) DEBUG(_ADD_LINENO(__FILE__, __LINE__, __VA_ARGS__))
#define _ADD_LINENO(f, ln, ...) "(" f ":" STRING(ln) ") " __VA_ARGS__

/**
 * Size of the receive queue.
 *
 * This value is a guess, I really don't know what to put here. The only problem
 * would be if we receive lots of packages in the short time it takes us to
 * answer an echo request.
 */
#define RECV_QUEUE_SIZE 16

#define RECV_BUFFER_SIZE 64

/** ECHO protocol runs in port 7 */
#define PORTNUM_ECHO 7

/* we don't need the whole shell just to call one command */
extern int _gnrc_netif_config(int argc, char **argv);

int main(void)
{
    msg_t msg_queue[RECV_QUEUE_SIZE];
    sock_udp_ep_t local_endp = { .port = PORTNUM_ECHO, .family = AF_INET6 };
    sock_udp_t sock;
    int sockerr;

    LOG("Starting\n");

    msg_init_queue(msg_queue, RECV_QUEUE_SIZE);
    if ((sockerr = sock_udp_create(&sock, &local_endp, NULL,
                                                    SOCK_FLAGS_REUSE_EP)) < 0) {
        LOG("Cannot create socket. Error (%d).\n", sockerr);
        goto server_failure;
    }

    LOG("Socket opened, your IP address\n");

/* TODO: set a proper address */
    //~ if (gnrc_netapi_set(iface, opt, 0, addr, addr_len) < 0) {
        //~ printf("[ECHO SRV] Warning: unable to set address!\n");
        //~ return 1;
    //~ }

    _gnrc_netif_config(0, NULL); /* print current network settings */

    while (1) {
        sock_udp_ep_t remote_ep;
        uint8_t recv_buff[RECV_BUFFER_SIZE];

        ssize_t n_recv = sock_udp_recv(&sock, recv_buff, sizeof(recv_buff),
                                      SOCK_NO_TIMEOUT, &remote_ep);

        /* FIXME: How do we know if the data was to big to fit in the buffer? */

        if (n_recv == 0) {
            LOG("Received nothing (\?\?)\n");
        } else if (n_recv < 0) {
            LOG("Reception error (%zd)\n", n_recv);
        } else {
            char remote_addr[IPV6_ADDR_MAX_STR_LEN];

            ipv6_addr_to_str(remote_addr, (ipv6_addr_t*)remote_ep.addr.ipv6,
                             sizeof(remote_addr));
            LOG("Received bytes: %zd\n", n_recv);
            LOG("Remote [%s]:%d.\n", remote_addr, remote_ep.port);

            ssize_t n_sent = sock_udp_send(&sock, recv_buff, n_recv, &remote_ep);
            if (n_sent < 0)
                LOG("Response failed: %zd\n", n_recv);

        }

    }

server_failure:
    LOG("Exiting server, this should not happen!\n");

    return 0;
}
