/**
 * @file
 * @brief       Some hacky debug macros
 *
 * @author      Juan I. Carrano <juan@carrano.com.ar>
 */

#ifndef LOG_H
#define LOG_H

#define ENABLE_DEBUG (1)
#include "debug.h"

#define STRING(s) #s
#define LOG(...) DEBUG(_ADD_LINENO(__FILE__, __LINE__, __VA_ARGS__))
#define _ADD_LINENO(f, ln, ...) "(" f ":" STRING(ln) ") " __VA_ARGS__

#endif /* LOG_H */
