/**
 * @file
 * @brief       Simple UDP echo server - Daemon
 *
 * @author      Juan I. Carrano <juan@carrano.com.ar>
 *
 */

#ifndef ECHOSERVER_H
#define ECHOSERVER_H

/**
 * Start the echo server at the specified port.
 *
 * This will spawn a new thread. If the server is already running nothing will
 * be done.
 *
 * Limitations:
 *
 * Only one instance of the echo server is currently supported.
 * Currently there is no way to stop the server.
 */
int echoserver_start(uint16_t port, uint32_t delay);

#endif /* ECHOSERVER_H */
