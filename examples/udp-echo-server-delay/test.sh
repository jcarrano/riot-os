#!/bin/sh

# Usage:
#	test.sh [echo-server-address [interface [port]]]
#

REMOTE_ADDR=$1
INTERFACE=${2:-tapbr0}
PORT=${3:-7}
LOCAL_PORT=30777

MY_ADDR=$(ip -6 -br addr show $INTERFACE  | sed -E 's/.* ([^ \/]+)\/.+$/\1/')

fail_test() {
	echo "Test failed:" $1
	return 1
}

ncat_cmd() {
	ncat -6u --send-only -p $LOCAL_PORT "${REMOTE_ADDR}%${INTERFACE}" $PORT
}

ncat_listen_cmd() {
	ncat -6u -i2 --recv-only -l "${MY_ADDR}%${INTERFACE}" $LOCAL_PORT
}

# This message is too bit and should be rejected
LONGMSG=38ca37a7cd836224f4d13074be922d8a6290b9bc601066b887a1d735ab81a06fcba003fb3bf1d39e

_send_long_messages() {
	(ncat_listen_cmd | tee /dev/stderr) &
	(echo $LONGMSG | ncat_cmd) &
	(echo ping | ncat_cmd) &
	sleep 0.1
	wait
}

long_message_test() {
	RESPONSE=$(_send_long_messages)

	[ x"$RESPONSE" = xping ] || fail_test "Long message"
}

SHORT_MESSAGES="38ca37a7cd83622
4f4d13074be922
d8a6290b9bc601066
b887a1d73
5ab81a06fcba00
3fb3bf1d39e"""

_send_short_messages() {
	(ncat_listen_cmd | tee /dev/stderr) &
	for m in $SHORT_MESSAGES ; do
		echo "Sending: $m" >&2
		(echo $m | ncat_cmd) &
		sleep 0.1
	done
	wait
}

short_messages_test() {
	#_send_short_messages
	RESPONSE=$(_send_short_messages)

	[ x"$RESPONSE" = x"$SHORT_MESSAGES" ] || fail_test "Short message"
}

long_message_test && short_messages_test && echo "Test Pass"

