examples/udp-echo-server-delay
==============================

A small ECHO server that delays packets before replaying.

This application echoes back any UDP packet received at port 7, after a 1 second
delay. It makes use of a queue to hold the packets while it waits for the time
send them back, so the amount of data that it can process per unit time is
limited by the size of the queue. Also, the maximum size of datagrams is limited.

For communication, it uses the udp sock interface. This probably results in more
memory copy operations and is less efficient than using the gnrc interface
directly, but it is easier to use.

Currently there is no way to stop the server process and for that reason the
shell is disabled as there is not much use to it.

Usage
=====

Use `shell` to build the app for the native platform and launch it.

On startup, the output of the ifconfig utility will be printed to the console.

By default the ECHO server logs lots of data to the console. Change the macro
`ENABLE_DEBUG` in `log.h` to modify this behavior.


Communicating
=============

The application does not set a proper ipv6 address. Use the link-local address
to communicate. Note that you must specify the interface when using link-local
addresses.

With netcat:
```
netcat -6u 'fe80::74b3:baff:fe20:ec27%tapbr0' 7
```

Of course, replace the address and tapbr0 for your actual address and interface.

Testing
=======

A simple test script (`test.sh`) is provided. It does two tests:

1. Send a message greater then supported. This should be rejected and not cause
   an error.
2. Send a sequence of short messages. They should be received back in order.

Note that in the real life the order of the responses could vary as there is
no guarantee in the ordering of packages. Over a tap interface and with an
imposed delay of 0.1 sec between requests it is not a problem.
