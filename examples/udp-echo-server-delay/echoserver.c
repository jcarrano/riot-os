/**
 * @file
 * @brief       Simple UDP echo server - Daemon
 *
 * @author      Juan I. Carrano <juan@carrano.com.ar>
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "thread.h"
#include "ringbuffer.h"
#include "xtimer.h"

#include "net/sock/udp.h"
#include "net/ipv6/addr.h"

#include "echoserver.h"
#include "log.h"

/**
 * Size of the receive queue.
 *
 * This value is a guess, I really don't know what to put here. The only problem
 * would be if we receive lots of packages in the short time it takes us to
 * answer an echo request.
 */
#define RECV_QUEUE_SIZE 8

/**
 * Size of the circular buffer for storing pending messages.
 *
 * The size of this buffer limits the number of requests that can be handled
 * per second. Once the buffer is full, further requests are ignored.
 */
#define PENDING_BUFFER_SIZE 2048

/**
 * This is the maximum size of the payload that we support.
 */
#define PAYL_BUFFER_SIZE 64

/* This is so we can declare out buffers locally in the server function */
#define EXTRA_DATA_SIZE (PENDING_BUFFER_SIZE + PAYL_BUFFER_SIZE \
                            + RECV_QUEUE_SIZE*sizeof(msg_t))

static char server_stack[THREAD_STACKSIZE_DEFAULT + EXTRA_DATA_SIZE];
static kernel_pid_t server_pid = KERNEL_PID_UNDEF;

/**
 * Header for storing size and due time for pending messages.
 */
struct pending_hdr {
    xtimer_ticks32_t due_time;
    sock_udp_ep_t remote_ep;
    size_t message_size;
};

struct echoserver_cfg {
    uint16_t port;
    uint32_t delay_usecs;
};

/**
 * Time addition.
 *
 * Note that this may overflow, but that's okay as long as we use the proper
 * time difference function.
 */
static xtimer_ticks32_t add_time(xtimer_ticks32_t t0, xtimer_ticks32_t t1)
{
    xtimer_ticks32_t r = {.ticks32 = t0.ticks32 + t1.ticks32};

    return r;
}

/**
 * Modular time difference.
 *
 * Compute future-present. As long as the two times did not overflow more than
 * one time, everything should be ok.
 */
static int32_t time_until(xtimer_ticks32_t present, xtimer_ticks32_t future)
{
    /* unsigned difference may overflow, but that is ok and then it is casted
     * as signed, resulting in the correct signed value.*/
    int32_t r = future.ticks32 - present.ticks32;

    return r;
}

/**
 * Convert a time difference into a positive ticks value.
 *
 * If d is negative it is clamped to zero.
 */
static xtimer_ticks32_t positive_ticks(int32_t d)
{
    xtimer_ticks32_t r;

    r.ticks32 = d < 0? 0 : d;

    return r;
}

/**
 * Maximum representable time
 */
static const xtimer_ticks32_t TICKS32_MAX = {UINT32_MAX};

/**
 * Schedule a packet for delivery.
 *
 * The value of data must be grater or equal to 1. Packets of length zero will
 * be silently discarded.
 *
 * @return  0 on success, Nonzero on failure.
 */
static int evbuffer_add(ringbuffer_t *__restrict rb,
                        xtimer_ticks32_t due_time,
                        void *data, size_t size,
                        sock_udp_ep_t *remote_ep)
{
    struct pending_hdr hdr = {due_time, *remote_ep, size};

    if (size == 0) {
        return 0;
    }

    if (ringbuffer_get_free(rb) < sizeof(hdr) + size) {
        return 1;
    }

    ringbuffer_add(rb, (char *)&hdr, sizeof(hdr));
    ringbuffer_add(rb, (void *)data, size);

    return 0;
}

static size_t min(size_t a, size_t b)
{
    return (a < b)? a : b;
}

/**
 * Check if next queued packet is due and return it if it is.
 *
 * @param       rb  ringbuffer instance
 * @param       curr_time   Current time.
 * @param       buf     The packet payload will be stored here
 * @param       buf_size     Size of the buf buffer.
 * @param[out]  remote_ep   The remote endpoint will be stored here.
 *
 * @return      Size of the packet. Zero means there is no packet available.
 *              If zero is return, nothing is written to remote_ep.
 */
static size_t evbuffer_get(ringbuffer_t *__restrict rb,
                        xtimer_ticks32_t curr_time, void *buf,
                        size_t buf_size,
                        sock_udp_ep_t *remote_ep)
{
    struct pending_hdr hdr;
    size_t bytes_read, bytes_removed;
    size_t hdr_read;

    hdr_read = ringbuffer_peek(rb, (char*)&hdr, sizeof(hdr));
    if (hdr_read != sizeof(hdr)) {
        /* Either there is a header or there is nothing */
        assert(hdr_read == 0);
        return 0;
    }

    if (time_until(curr_time, hdr.due_time) > 0) {
        return 0;
    }

    /* Up to this point we didn't modify the queue */
    ringbuffer_remove(rb, sizeof(hdr));

    /* This is a bit more complicated than it could be. If we know that we never
     * insert more than X bytes in the buffer and we know that buf is always X
     * bytes, then buf_size is not required and this logic can be ommited, but
     * then we would have some dangerous code
     */
    bytes_read = ringbuffer_peek(rb, buf, min(buf_size, hdr.message_size));
    bytes_removed = ringbuffer_remove(rb, hdr.message_size);

    *remote_ep = hdr.remote_ep;

    /* if this fails then we corrupted our buffer*/
    assert(bytes_removed == hdr.message_size);

    return bytes_read;
}

/**
 * Return the due time for the next unprocessed packet.
 *
 * If there are no packets, returns the maximum representable time. This is
 * ambiguous, so you MUST check whether the buffer is empty!
 */
static xtimer_ticks32_t evbuffer_next_due(ringbuffer_t *__restrict rb)
{
    struct pending_hdr hdr;

    if (ringbuffer_peek(rb, (char*)&hdr, sizeof(hdr)) != sizeof(hdr)) {
        return TICKS32_MAX;
    }

    return hdr.due_time;
}

/**
 * Print the size of the data and the remote endpoint.
 */
static void log_tx(size_t n_data, sock_udp_ep_t *ep)
{
    char addr[IPV6_ADDR_MAX_STR_LEN];

    ipv6_addr_to_str(addr, (ipv6_addr_t*)ep->addr.ipv6, sizeof(addr));
    DEBUG("bytes: %zd from [%s]:%d.\n", n_data, addr, ep->port);
}

/**
 * Echo server function.
 *
 * This works by creating a queue (implemented with a circular buffer).
 * The incoming packets are inserted into the queue along with the time at which
 * it should be sent back.
 */
static void * _server_thread(void *args)
{
    /* is it OK to allocate this in the stack???? */
    msg_t msg_queue[RECV_QUEUE_SIZE];
    char _buffer_data[PENDING_BUFFER_SIZE];
    ringbuffer_t pending_buf = RINGBUFFER_INIT(_buffer_data);

    msg_t set_port_num, set_port_ack = {.type = 0, .content.value = 0};

    sock_udp_ep_t local_endp = { .port = 0, .family = AF_INET6 };
    sock_udp_t sock;
    int sockerr;

    (void)(args);

    LOG("Starting, receiving parameters\n");

    msg_init_queue(msg_queue, RECV_QUEUE_SIZE);

    msg_receive(&set_port_num);
    struct echoserver_cfg *cfg = set_port_num.content.ptr;
    local_endp.port = cfg->port;
    xtimer_ticks32_t reply_delay = xtimer_ticks_from_usec(cfg->delay_usecs);

    LOG("Received. I will run on port %u. Delay is %u\n", local_endp.port,
                                                          reply_delay.ticks32);

    msg_reply(&set_port_num, &set_port_ack);

    if ((sockerr = sock_udp_create(&sock, &local_endp, NULL,
                                                    SOCK_FLAGS_REUSE_EP)) < 0) {
        LOG("Cannot create socket. Error (%d).\n", sockerr);
        goto server_failure;
    }

    LOG("Entering loop\n");

    while (1) {
        sock_udp_ep_t remote_ep;
        char payload[PAYL_BUFFER_SIZE];

        uint32_t time_to_next_event = ringbuffer_empty(&pending_buf)?
                    SOCK_NO_TIMEOUT
                :   xtimer_usec_from_ticks(positive_ticks(
                    time_until(xtimer_now(), evbuffer_next_due(&pending_buf))));

        ssize_t n_recv = sock_udp_recv(&sock, payload, sizeof(payload),
                                      time_to_next_event, &remote_ep);
        xtimer_ticks32_t now = xtimer_now();

        /* FIXME: How do we know if the data was to big to fit in the buffer? */

        /* Insert the received package into the queue */
        if (n_recv == 0) {
            LOG("Received nothing (\?\?)\n");
        } else if (n_recv < 0) {
            if (n_recv != -ETIMEDOUT)
                LOG("Reception error (%zd)\n", n_recv);
        } else if (n_recv > 0) {
            LOG("Received ");
            log_tx(n_recv, &remote_ep);

            int add_fail = evbuffer_add(&pending_buf, add_time(now, reply_delay),
                                        payload, n_recv, &remote_ep);

            if (add_fail) {
                LOG("Warning: packet rejected\n");
            }
        }

        /* Processed expired items from the queue */
        while (1) {
            now = xtimer_now(); /* All that logging might incur delay, lets
                                   measure again */
            size_t to_send = evbuffer_get(&pending_buf, now, payload,
                                            sizeof(payload),  &remote_ep);

            if (to_send == 0) {
                break;
            }

            LOG("Sending ");
            log_tx(to_send, &remote_ep);

            ssize_t n_sent = sock_udp_send(&sock, payload, to_send, &remote_ep);
            if (n_sent < 0)
                LOG("Response failed: %zd\n", n_recv);
        }
    }

server_failure:
    LOG("Exiting server, this should not happen!\n");

    return NULL;
}

int echoserver_start(uint16_t port, uint32_t delay_usecs)
{
    int r = 0;

    if (server_pid <= KERNEL_PID_UNDEF ||
         thread_getstatus(server_pid) <= STATUS_STOPPED) {

        struct echoserver_cfg cfg = {.port=port, .delay_usecs=delay_usecs};
        msg_t set_port_ack, set_port_num = {.type=0, .content.ptr = &cfg};


        server_pid = thread_create(server_stack, sizeof(server_stack),
                      THREAD_PRIORITY_MAIN - 1,
                      THREAD_CREATE_STACKTEST, _server_thread, NULL,
                      "UDP Echo server");

        if (server_pid <= KERNEL_PID_UNDEF) {
            LOG("Failed to launch server thread\n");
            r = -1;
            goto server_start_fail;
        }

        if (!msg_send_receive(&set_port_num, &set_port_ack, server_pid)) {
            LOG("Cannot get response from server\n");
            r = -1;
            goto server_start_fail;
        }

        LOG("Server is up\n");
    } else {
        LOG("Server thread is already running\n");
    }

server_start_fail:

    return r;
}
