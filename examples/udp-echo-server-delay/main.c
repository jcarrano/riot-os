/**
 * @file
 * @brief       UDP echo server with delay - Entry
 *
 * @author      Juan I. Carrano <juan@carrano.com.ar>
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "thread.h"

#include "log.h"
#include "echoserver.h"

/** ECHO protocol runs in port 7 */
#define PORTNUM_ECHO 7
#define DELAY_USECS (1000*1000)

#define MAIN_QUEUE_SIZE 8

/* we don't need the whole shell just to call one command */
extern int _gnrc_netif_config(int argc, char **argv);

int main(void)
{
    msg_t msg_queue[MAIN_QUEUE_SIZE];

    LOG("Starting program & launching daemon\n");

    msg_init_queue(msg_queue, MAIN_QUEUE_SIZE);

    if (echoserver_start(PORTNUM_ECHO, DELAY_USECS) < 0) {
        LOG("Error launching daemon\n");
        goto main_failure;
    }

    LOG("Your IP address is\n");

/* TODO: set a proper address */
    //~ if (gnrc_netapi_set(iface, opt, 0, addr, addr_len) < 0) {
        //~ printf("[ECHO SRV] Warning: unable to set address!\n");
        //~ return 1;
    //~ }

    _gnrc_netif_config(0, NULL); /* print current network settings */

    /* sleep forever */
    while (1) {
        thread_sleep();
    }

main_failure:
    LOG("Exiting, this should not happen!\n");

    return 0;
}
